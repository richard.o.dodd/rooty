use chrono::NaiveDate;
use rooty::{NotFound, Routes};
use std::str::FromStr;

#[derive(Debug, Routes)]
pub enum MyRoutes {
    #[route = "/"]
    Home,
    #[route = "/about"]
    About,
    #[route = "/users/{id}"]
    User { id: i32 },
    //#[route = "/posts/{year}/{month}/{day}"]
    //Posts { year: i32, month: u8, day: u8 },
    #[route = "/posts/{date}"]
    Posts { date: NaiveDate },
    #[route = "/post/{title}"]
    Post { title: String },
}

fn main() {
    println!("home: {}", MyRoutes::Home.url());
    println!("about: {}", MyRoutes::About.url());
    println!("user: {}", MyRoutes::User { id: 32 }.url());
    /*
    println!(
        "posts: {}",
        MyRoutes::Posts {
            year: 2016,
            month: 1,
            day: 12,
        }
        .url()
    );
    */
    println!(
        "posts: {}",
        MyRoutes::Posts {
            date: NaiveDate::from_str("2018-12-11").unwrap()
        }
        .url()
    );
    println!(
        "post: {}",
        MyRoutes::Post {
            title: "my_post_title".into()
        }
        .url()
    );
    println!("{:?}", MyRoutes::parse_url("/post/my_post"));
    println!("{:?}", MyRoutes::parse_url("/posts/2012-12-07"));
}
