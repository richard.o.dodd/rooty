//! A rust crate for writying your web routes declaratively, and then parsing and formatting urls.
//!
//! For instructions for the proc macro, see the documentation for `rooty_derive::Routes`.
//!
//! The trait adds some minimal wrapping around parsing and formatting, in case you want to
//! implement `Display` and `FromStr` differently for your `Routes` type.
//!
//! # Examples
//!
//! Full example
//!
//! ```
//! use chrono::NaiveDate;
//! use rooty::{NotFound, Routes};
//! use std::str::FromStr;
//!
//! #[derive(Debug, Routes)]
//! pub enum MyRoutes {
//!     #[route = "/"]
//!     Home,
//!     #[route = "/about"]
//!     About,
//!     #[route = "/users/{id}"]
//!     User { id: i32 },
//!     #[route = "/posts/{date}"]
//!     Posts { date: NaiveDate },
//!     #[route = "/post/{title}"]
//!     Post { title: String },
//! }
//!
//! fn main() {
//!     assert_eq!(MyRoutes::Home.url().to_string(), "/");
//!     assert_eq!(MyRoutes::About.url().to_string(), "/about");
//!     assert_eq!(MyRoutes::User { id: 32 }.url().to_string(), "/users/32");
//!     assert_eq!(
//!         MyRoutes::Posts {
//!             date: NaiveDate::from_str("2018-12-11").unwrap()
//!         }
//!         .url().to_string(),
//!         "/posts/2018-12-11"
//!     );
//!     assert_eq!(
//!         MyRoutes::Post {
//!             title: "my_post_title".into()
//!         }
//!         .url().to_string(),
//!         "/post/my_post_title"
//!     );
//!     println!("{:?}", MyRoutes::parse_url("/post/my_post"));
//!     println!("{:?}", MyRoutes::parse_url("/posts/2012-12-07"));
//!     assert!(MyRoutes::parse_url("/a/made/up/url").is_err());
//! }
//! ```

pub use rooty_derive::Routes;
#[doc(hidden)]
pub use rooty_shared::{consume_literal, consume_placeholder};
pub use rooty_shared::{NotFound, Routes};
